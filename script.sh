
mkdir rapports

#Gitinspector produit des rapports sous forme de tableaux ou graphiques absolus mettant
#en évidence la contribution quantitative de chaque auteur du dépôt. Il permet aussi de choisir
#le type de fichiers à analyser lors de la production de ses rapports, permettant d’éviter le biais
#impliqué par la présence dans le dépôt de fichiers générés automatiquement. Malheureusement,
#il ne produit pas de graphique temporel mettant en évidence la régularité de chaque auteur
#du dépôt Git.

git clone https://github.com/ejwa/gitinspector.git inspector
cd inspector

./gitinspector.py --format=html https://github.com/mockito/mockito.git > ../rapports/inspectin_general.html
./gitinspector.py --format=html --grading=true https://github.com/mockito/mockito.git > ../rapports/inspectin_complet.html

./gitinspector.py --format=html --grading=true -x "author:^(?!(Szczepan Faber))" https://github.com/mockito/mockito.git > ../rapports/stat_Szczepan_Faber.html
./gitinspector.py --format=html --grading=true -x "author:^(?!(Brice))" https://github.com/mockito/mockito.git > ../rapports/stat_Brice_Dutheil.html
./gitinspector.py --format=html --grading=true -x "author:^(?!(Tim van der Lippe))" https://github.com/mockito/mockito.git > ../rapports/stat_Tim_van_der_Lippe.html
./gitinspector.py --format=html --grading=true -x "author:^(?!(Christian Schwarz))" https://github.com/mockito/mockito.git > ../rapports/stat_Christian_Schwarz.html
./gitinspector.py --format=html --grading=true -x "author:^(?!(Rafael Winterhalter))" https://github.com/mockito/mockito.git > ../rapports/stat_Rafael_Winterhalter.html

